Submission for programming challenge by The Secret Police.

My solution works using a custom ImageGrid component that extracts texture data from each sprite in the Sprites folder and applies them to a set of RawImage objects using Texture2D SetPixels and GetPixels. This results in increased load time on startup, but silky smooth performance while scrolling.

A callback delegate on ImageGrid (OnGridEntryClicked) currently replicates the original sample functionality (indicates button click with a graphic effect and logs the sprite number to the console), but this can obviously be changed or extended as needed.

Surprisingly, instantiating an associated text object for each grid entry has little noticable effect on performance (uncheck the "Show Text" boolean on ImageGrid to compare with and without text), so I've left this functionality mostly untouched.


Vertical scrolling is implemented using a stripped-down version of Unity's ScrollRect ("SpeedyScroller") that clamps the content object between a max and min y-value rather than recalculating bounds every frame.

Tested running at consistent 30 FPS on my Galaxy S7, but curious to hear how performance compares on other devices.