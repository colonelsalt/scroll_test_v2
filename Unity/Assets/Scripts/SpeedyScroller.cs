﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SpeedyScroller : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IInitializePotentialDragHandler
{
    // --------------------------------------------------------------

    [SerializeField]
    private RectTransform _content;

    [SerializeField]
    private float _decelerationRate = 0.135f;

    // --------------------------------------------------------------

    private float _dragStartPos = 0f;

    private float _contentPosOnDragStart = 0f;

    private float _prevContentPos = 0f;

    private float _scrollVelocity = 0f;

    private bool _isDragging = false;

    // --------------------------------------------------------------


    private void Awake()
    {
        if (_content == null)
        {
            if (transform.childCount > 0)
            {
                _content = transform.GetChild(0) as RectTransform;
            }
            else
            {
                Debug.LogWarning("SpeedyScroller: No content assigned.");
                gameObject.SetActive(false);
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _dragStartPos = eventData.position.y;
        _contentPosOnDragStart = _content.anchoredPosition.y;
        _isDragging = true;
    }

    // Reset velocity at start of every drag
    public void OnInitializePotentialDrag(PointerEventData eventData)
    {
        _scrollVelocity = 0f;
    }

    public void OnDrag(PointerEventData eventData)
    {
        float dragDelta = eventData.position.y - _dragStartPos;

        // Position we want to move content to (based on current drag amount)
        float desiredPos = Mathf.Clamp(_contentPosOnDragStart + dragDelta, -_content.sizeDelta.y, 0f);

        _content.anchoredPosition = new Vector2(_content.anchoredPosition.x, desiredPos);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _isDragging = false;
    }

    private void LateUpdate()
    {
        float deltaTime = Time.unscaledDeltaTime;

        // If user not currently dragging, but we still have velocity, apply inertia
        if (!_isDragging && Mathf.Abs(_scrollVelocity) > 0f)
        {
            float contentPos = _content.anchoredPosition.y;
            _scrollVelocity *= Mathf.Pow(_decelerationRate, deltaTime);

            // Stop movement if velocity too low
            if (Mathf.Abs(_scrollVelocity) < 1f) _scrollVelocity = 0f;

            contentPos = Mathf.Clamp(contentPos + (_scrollVelocity * deltaTime), -_content.sizeDelta.y, 0f);

            _content.anchoredPosition = new Vector2(_content.anchoredPosition.x, contentPos);
        }

        else if (_isDragging)
        {
            float newVelocity = (_content.anchoredPosition.y - _prevContentPos) / deltaTime;
            _scrollVelocity = Mathf.Lerp(_scrollVelocity, newVelocity, deltaTime * 10f);
        }

        _prevContentPos = _content.anchoredPosition.y;
    }

}
