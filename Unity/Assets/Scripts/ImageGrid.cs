﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Events;

public class ImageGrid : MonoBehaviour, IPointerClickHandler
{
    // --------------------------------------------------------------

    // Speculative max. size for Unity's Texture2D (determines no. of textures needed to render entire grid)
    private const int MAX_TEXTURE_HEIGHT = 10000;

    [Serializable]
    public class ImageGridEvent : UnityEvent<int> { }

    // --------------------------------------------------------------

    [SerializeField]
    private bool _showText = true;

    [SerializeField]
    private GameObject _textPrefab;

    [SerializeField]
    private Sprite[] _spriteList;
    
    [SerializeField]
    // Width and height of each sprite entry to display in grid
    private Vector2 _imageSize;

    [SerializeField]
    // Spacing between each grid entry
    private int _verticalSpacing, _horizontalSpacing;

    [SerializeField]
    // Callback to fire when entry in grid clicked
    private ImageGridEvent _onGridEntryClicked = new ImageGridEvent();

    // --------------------------------------------------------------

    // Width and height of our RectTransform before scaling
    private int _viewWidth, _viewHeight;

    // No. of seperate textures needed to render entire grid (constrained by MAX_TEXTURE_SIZE)
    private int _numTextures = 0;

    // Height of each texture used to render each grid section (largest multiple of GridUnitY < MAX_TEXTURE_SIZE)
    private int _textureHeight = 0;

    // Ref. to button overlay object to indicate user's screen clicks
    private Image _buttonImage;

    // RectTransform of button overlay object
    private RectTransform _buttonTransform;

    // RawImages used to render grid to screen
    private RawImage[] _gridImages;

    // Dimensions (in grid units) of section of grid visible in view at any given time
    private int _numRows = 0, _numColumns = 0;

    // Ref. to this object's RectTransform
    private RectTransform _rectTransform;

    private Camera _mainCam;

    // --------------------------------------------------------------
    // Helper properties

    // Width in pixels of a single entry in the table
    private int GridUnitX
    {
        get
        {
            return (int)_imageSize.x + _horizontalSpacing;
        }
    }

    // Height in pixels of a single entry in table
    private int GridUnitY
    {
        get
        {
            return (int)_imageSize.y + _verticalSpacing;
        }
    }

    // Total height (in pixels) of the entire grid
    private int GridHeightTotal
    {
        get
        {
            return GridUnitY * _spriteList.Length / _numColumns;
        }
    }

    // --------------------------------------------------------------

    private void Awake()
    {
        _mainCam = Camera.main;
        _rectTransform = GetComponent<RectTransform>();
        _buttonImage = GetComponentInChildren<Image>();
        _buttonTransform = _buttonImage.GetComponent<RectTransform>();

        // Setup methods
        CalculateGridSize();
        CreateGridSections();
        ScaleGridSize();

        DrawGrid();
    }

    private void CalculateGridSize()
    {
        _viewWidth = (int)_rectTransform.rect.width;
        _viewHeight = (int)_rectTransform.rect.height;

        // Determine no. of rows and columns that can fit on screen at once
        int tempWidth = _horizontalSpacing / 2;
        while (tempWidth < _viewWidth)
        {
            _numColumns++;
            tempWidth += GridUnitX;
        }
        int tempHeight = _verticalSpacing;
        while (tempHeight < _viewHeight)
        {
            _numRows++;
            tempHeight += GridUnitY;
        }

        // Texture height is highest multiple of GridUnitY <= MAX_TEXTURE_HEIGHT
        int numUnitsVisible = MAX_TEXTURE_HEIGHT / GridUnitY;
        _textureHeight = numUnitsVisible * GridUnitY;

        // No. of textures needed to fill grid is smallest multiple of texture height >= total grid height
        _numTextures = Mathf.CeilToInt((float)GridHeightTotal / _textureHeight);
    }

    // Spawn RawImage objects to display each section of the grid
    private void CreateGridSections()
    {
        _gridImages = new RawImage[_numTextures];
        for (int i = 0; i < _numTextures; i++)
        {
            GameObject imageObject = new GameObject("Grid Image " + (i + 1));
            RectTransform imageRect = imageObject.AddComponent<RectTransform>();

            // Set anchor for each new image to top-centre
            imageRect.anchorMin = new Vector2(0.5f, 1);
            imageRect.anchorMax = new Vector2(0.5f, 1);

            // Child image to grid's RectTransform and scale it to calculated texture height
            imageRect.SetParent(_rectTransform);
            imageRect.localScale = Vector3.one;
            imageRect.sizeDelta = new Vector2(_viewWidth, _textureHeight);

            // Move image to bottom of previous image (for seamless scrolling between them)
            imageRect.anchoredPosition = new Vector2(0f, (-_textureHeight / 2) - (_textureHeight * i));
            imageRect.localPosition = new Vector3(imageRect.localPosition.x, imageRect.localPosition.y, 0f);

            _gridImages[i] = imageObject.AddComponent<RawImage>();
        }
    }

    private void ScaleGridSize()
    {
        // Scale this object's RectTransform to fill entire grid
        _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, GridHeightTotal - (GridUnitY * (_numRows - 0.5f)));
        _rectTransform.anchoredPosition = new Vector2(0f, -_rectTransform.sizeDelta.y);

        // Scale button overlay object to overlap exactly one grid entry
        _buttonTransform.sizeDelta = new Vector2(GridUnitX, GridUnitY);
        _buttonTransform.SetAsLastSibling();
        _buttonImage.enabled = false;
    }

    // Applies all textures from SpriteList to their appropriate grid section image
    private void DrawGrid()
    {
        // Start with transparent texture for texture background
        Texture2D drawTexture = GetTransparentTexture();

        int drawPosX = _horizontalSpacing / 2;
        int drawPosY = _textureHeight - (int)_imageSize.y - (_verticalSpacing / 2);

        // Index of raw image to apply texture to
        int rawImageIndex = 0;

        for (int i = 0; i < _spriteList.Length; i++)
        {
            // If we've reached end of row, shift down 1 column
            if (drawPosX + (int)_imageSize.x > _viewWidth)
            {
                drawPosX = _horizontalSpacing / 2;
                drawPosY -= GridUnitY;
            }

            // If we've reached bottom of current Raw Image, apply texture and move to next image
            if (drawPosY < 0)
            {
                // Apply SetPixel changes to texture and assign it to our current RawImage
                drawTexture.Apply();
                _gridImages[rawImageIndex].texture = drawTexture;

                // Start again with transparent background for next texture
                drawTexture = GetTransparentTexture();
                drawPosY = _textureHeight + drawPosY;
                rawImageIndex++;
            }

            // Copy texture from sprite[i] and apply it to appropriate grid position in texture
            drawTexture.SetPixels(drawPosX, drawPosY, (int)_imageSize.x, (int)_imageSize.y, _spriteList[i].texture.GetPixels());

            if (_showText)
            {
                // Spawn text prefab and place it on top of most recently drawn grid entry
                GameObject textObject = Instantiate(_textPrefab, _gridImages[rawImageIndex].transform);
                textObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(drawPosX - (_viewWidth / 2) + ((int)_imageSize.x / 2), drawPosY);
                textObject.GetComponent<Text>().text = i.ToString();
            }

            drawPosX += GridUnitX;
        }

        // Apply and assign texture draws to final RawImage in grid
        drawTexture.Apply();
        _gridImages[rawImageIndex].texture = drawTexture;

    }

    // Returns transparent texture corresponding to width and height of each image texture
    public Texture2D GetTransparentTexture()
    {
        Texture2D transparentTexture = new Texture2D(_viewWidth, _textureHeight);
        Color[] transparent = new Color[_viewWidth * _textureHeight];
        for (int i = 0; i < _viewWidth * _textureHeight; i++)
        {
            transparent[i] = new Color(0f, 0f, 0f, 0f);
        }
        transparentTexture.SetPixels(transparent);
        return transparentTexture;
    }

    // Indicate which grid entry user clicked
    public void OnPointerClick(PointerEventData eventData)
    {
        // Get 2D index of grid 
        Vector2 gridEntry = ScreenSpaceToGridCoordinate(eventData.position);

        int entryX = (int)gridEntry.x;
        int entryY = (int)gridEntry.y;

        // Move button overlay graphic above correct grid entry
        _buttonTransform.anchoredPosition = new Vector2(entryX * GridUnitX, ((-entryY - 1) * GridUnitY));
        _buttonImage.enabled = true;

        // Invoke button click callback
        _onGridEntryClicked.Invoke((entryX + (entryY * _numColumns)));
    }

    // Convert screen space coordinate to 2D grid entry coordinate of format: (row, column)
    private Vector2 ScreenSpaceToGridCoordinate(Vector2 screenPos)
    {
        // Convert position to local coordinate relative to grid's RectTransform
        Vector2 rectPos = Vector2.zero;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, screenPos, _mainCam, out rectPos);

        // Set origin to top left corner
        float realY = _rectTransform.rect.height - rectPos.y;
        rectPos.Set(rectPos.x, realY);

        // Scale coordinates by grid entry dimensions to generate grid coordinates
        return new Vector2(rectPos.x / GridUnitX, rectPos.y / GridUnitY);
    }

}
