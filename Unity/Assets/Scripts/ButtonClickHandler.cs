﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonClickHandler : MonoBehaviour
{

	public void OnGridEntryClicked(int entry)
    {
        Debug.Log("Thumbnail clicked: " + entry);
    }

}
